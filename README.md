
# VSCODE Devcontainer

A Centos 7 Container that simulates the dataterminal or gocd agent

## Description

Us this project as a submodule inside your VScode project.
It will provide all the specs specified below.
You whould beable to spint up everything you need and run your scripts just as if you are running in production.

## Getting Started
* You will need VScode, Docker and the "Remote Development Extension"

### Specs
    - Centos 7 Workspace
    - Python 3.8
    - Sqitch 0.9996
    - PGadmin4
    - Postgres 9.6

* Describe any prerequisites, libraries, OS version, etc., needed before installing program.
* ex. Windows 10

### Installing
* Open up your project folder in VSCode
* Open up the terminal inside of VSCode
* Run this inside of your project folder

    ```git submodule add https://github.cfpb.gov/nguyenhu/vscode-devcontainer.git  .devcontainer```
 

### Helpful Utilities 

* How to Package your script into 1 file:
    (Assuming you have no special dependencies)
    https://pyinstaller.readthedocs.io/en/stable/operating-mode.html

    - Direct Python Script:
        ```
        pyinstaller <python_script.py>  --onefile

        ```
    - Using spec file:
        ```
        pyinstaller python_file_name.spec --distpath=<directory to put file>  --onefile
        ```

    - Mockup PPAS wrappers:
        ```
        cd .devcontainer
        make ppas
        ```
 

## Authors

Contributors names and contact info

Hung Nguyen 
hung.nguyen@cfpb.gov
  