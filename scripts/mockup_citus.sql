-- I only copied the wrappers that we used in our sqitch migration so the same script should work inside our
-- container environment
create role operational_dba;
create role postgres;
ALTER USER postgres WITH SUPERUSER;
ALTER USER postgres WITH LOGIN;

CREATE SCHEMA IF NOT EXISTS op_dba
    AUTHORIZATION postgres;
 

CREATE OR REPLACE FUNCTION public.run_command_on_workers(
 	t text )
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    
AS $BODY$
 
BEGIN
PERFORM 'select 1';
END
$BODY$;

CREATE OR REPLACE FUNCTION public.create_distributed_table(
 	t text, col text )
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    
AS $BODY$
 
BEGIN
PERFORM 'select 1';
END
$BODY$;
 

ALTER FUNCTION public.create_distributed_table(text,text)
    OWNER TO postgres;

ALTER FUNCTION public.run_command_on_workers(text)
    OWNER TO postgres;
