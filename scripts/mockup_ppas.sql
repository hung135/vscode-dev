--begin;
create schema op_dba;
create role operational_dba;
create role postgres;
ALTER USER operational_dba WITH SUPERUSER;
create user postgres with PASSWORD 'docker';
ALTER USER postgres WITH SUPERUSER;
ALTER USER postgres WITH LOGIN;
create EXTENSION dblink; 
-- DROP FUNCTION op_dba.cfpb_ls_dir(character varying);

 DROP TYPE op_dba.cfpb_ls_dir_type cascade;

CREATE TYPE op_dba.cfpb_ls_dir_type AS
   (filename character varying(2000),
    file_size character varying(20),
    access_date date,
    modified_date date,
    is_directory character varying(5));
ALTER TYPE op_dba.cfpb_ls_dir_type
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION op_dba.cfpb_ls_dir(dir_path character varying)
  RETURNS SETOF op_dba.cfpb_ls_dir_type AS
$BODY$
declare
dir op_dba.cfpb_ls_dir_type%rowtype;
file_dir varchar(2000);
begin
-- file_dir:=replace(dir_path,'/..','');
-- file_dir:='/home/dtwork/dw/file_transfers/'||file_dir;
-- for dir in (SELECT
--             pg_ls_dir AS filename,
--             pg_size_pretty((pg_stat_file(pg_ls_dir)).size) file_size,
--             pg_stat_file(pg_ls_dir).access access_date,
--             pg_stat_file(pg_ls_dir).modification modified_date,
--             pg_stat_file(pg_ls_dir).isdir is_directory
--             FROM  pg_ls_dir(file_dir)
--            where pg_stat_file(file_dir).isdir=false)
-- loop
-- return next dir;
-- end loop;
-- return;

perform "select 1";
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION op_dba.cfpb_ls_dir(character varying)
  OWNER TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.cfpb_ls_dir(character varying) TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.cfpb_ls_dir(character varying) TO operational_dba;
REVOKE ALL ON FUNCTION op_dba.cfpb_ls_dir(character varying) FROM public;


DROP FUNCTION op_dba.cfpb_read_file(character varying);

CREATE OR REPLACE FUNCTION op_dba.cfpb_read_file(dir_path character varying)
  RETURNS text AS
$BODY$
declare
data text;
file_dir varchar(2000);
begin
file_dir:=replace(dir_path,'/..','');
file_dir:='file_transfers/'||file_dir;
data:=pg_read_file(file_dir);
return data;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION op_dba.cfpb_read_file(character varying)
  OWNER TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.cfpb_read_file(character varying) TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.cfpb_read_file(character varying) TO operational_dba;
REVOKE ALL ON FUNCTION op_dba.cfpb_read_file(character varying) FROM public;


-- FUNCTION: op_dba.copy_from_host(character varying, character varying, character varying)
-- DROP FUNCTION op_dba.copy_from_host(character varying, character varying, character varying);
CREATE OR REPLACE FUNCTION op_dba.copy_from_host(
	dest_option character varying,
	source_file character varying,
	import_option character varying DEFAULT ''::character varying)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE SECURITY DEFINER PARALLEL UNSAFE
AS $BODY$
declare
v_sql varchar(2000);
header_csv_quote varchar(100);

BEGIN

If dest_option is NULL
then
RAISE EXCEPTION 'No Table given'  USING HINT ='Please provide target table using the dest_option parameter!';
elsif source_file is NULL
then
RAISE EXCEPTION 'No source file given'  USING HINT ='Please provide source file using the source_file parameter!';
else
v_sql := 'COPY '||dest_option||' from ''/home/dtwork/dw/file_transfers/'||source_file||''' '||import_option;
RAISE NOTICE ' % ', v_sql;
execute v_sql;
end if;
END
$BODY$;

ALTER FUNCTION op_dba.copy_from_host(character varying, character varying, character varying)
    OWNER TO postgres;

GRANT EXECUTE ON FUNCTION op_dba.copy_from_host(character varying, character varying, character varying) TO operational_dba;
GRANT EXECUTE ON FUNCTION op_dba.copy_from_host(character varying, character varying, character varying) TO postgres;
REVOKE ALL ON FUNCTION op_dba.copy_from_host(character varying, character varying, character varying) FROM PUBLIC;
                                   
DROP FUNCTION op_dba.copy_to_host(character varying, character varying);

CREATE OR REPLACE FUNCTION op_dba.copy_to_host(
    source_option character varying,
    dest_file character varying)
  RETURNS void AS
$BODY$
BEGIN
If source_option is NULL
then
RAISE EXCEPTION 'No source provided'  USING HINT ='Please provide target table or  query using the source_option parameter!';
elsif dest_file is NULL
then
RAISE EXCEPTION 'No destination file provided'  USING HINT ='Please provide destination file using the dest_file parameter!';
else
EXECUTE 'COPY '||source_option||' to ''/home/dtwork/dw/file_transfers/'||dest_file||''' WITH DELIMITER '','' CSV HEADER';
end if;
END
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION op_dba.copy_to_host(character varying, character varying)
  OWNER TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.copy_to_host(character varying, character varying) TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.copy_to_host(character varying, character varying) TO operational_dba;
REVOKE ALL ON FUNCTION op_dba.copy_to_host(character varying, character varying) FROM public;


DROP FUNCTION op_dba.create_database(character varying, character varying);

CREATE OR REPLACE FUNCTION op_dba.create_database(
    database_name character varying,
    template_name character varying)
  RETURNS void AS
$BODY$
DECLARE
opdba_privileges_action varchar(255);
opdba_privileges_action_revoke varchar(255);
public_role varchar(255);
public_privilege varchar(255);
BEGIN
opdba_privileges_action:='GRANT ALL PRIVILEGES ON DATABASE ' || quote_ident(database_name) || ' TO operational_dba' ;
opdba_privileges_action_revoke:='REVOKE ALL ON DATABASE ' || quote_ident(database_name) || ' FROM public' ;
public_role:= 'CREATE ROLE ' || quote_ident(database_name) || ' WITH NOSUPERUSER NOCREATEDB NOLOGIN NOCREATEROLE INHERIT';
public_privilege:= 'GRANT CONNECT ON  database ' || quote_ident(database_name) || ' to ' ||quote_ident(database_name);
IF EXISTS (SELECT 1 FROM pg_database WHERE datname = database_name) THEN
      RAISE NOTICE 'Database already exists';
   ELSE
      PERFORM dblink_exec( 'dbname='||current_database(), 'CREATE DATABASE ' || quote_ident(database_name));
   EXECUTE opdba_privileges_action;
      RAISE NOTICE  '%' , opdba_privileges_action;
   EXECUTE opdba_privileges_action_revoke;
      RAISE NOTICE  '%' , opdba_privileges_action_revoke;
   EXECUTE public_role;
      RAISE NOTICE  '%' , public_role;
   EXECUTE public_privilege;
      RAISE NOTICE  '%' , public_privilege;
   EXECUTE 'GRANT ' || quote_ident(database_name) || ' to operational_dba WITH ADMIN OPTION';
END IF;
END
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION op_dba.create_database(character varying, character varying)
  OWNER TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.create_database(character varying, character varying) TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.create_database(character varying, character varying) TO operational_dba;
REVOKE ALL ON FUNCTION op_dba.create_database(character varying, character varying) FROM public;


DROP FUNCTION op_dba.create_extension(character varying, character varying);

CREATE OR REPLACE FUNCTION op_dba.create_extension(
    extension_name character varying,
    schema_name character varying)
  RETURNS void AS
$BODY$
BEGIN
EXECUTE 'CREATE EXTENSION  IF NOT EXISTS '|| quote_ident(extension_name) ||' WITH  SCHEMA '|| quote_ident(schema_name) ;
END
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION op_dba.create_extension(character varying, character varying)
  OWNER TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.create_extension(character varying, character varying) TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.create_extension(character varying, character varying) TO operational_dba;
REVOKE ALL ON FUNCTION op_dba.create_extension(character varying, character varying) FROM public;


DROP FUNCTION op_dba.create_role(character varying);

CREATE OR REPLACE FUNCTION op_dba.create_role(role_name character varying)
  RETURNS void AS
$BODY$
declare

check_role int;

BEGIN

select count(*) into check_role from pg_roles where rolname = lower(role_name);

if check_role = 0
then
EXECUTE 'CREATE ROLE ' || quote_ident(role_name) || ' WITH NOSUPERUSER NOCREATEDB NOLOGIN NOCREATEROLE INHERIT';
EXECUTE 'GRANT ' || quote_ident(role_name) || ' to operational_dba';
else
RAISE NOTICE 'Database role %  already exists ', role_name;
end if;

END
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION op_dba.create_role(character varying)
  OWNER TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.create_role(character varying) TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.create_role(character varying) TO operational_dba;
REVOKE ALL ON FUNCTION op_dba.create_role(character varying) FROM public;


DROP FUNCTION op_dba.define_template(character varying, boolean, boolean);

CREATE OR REPLACE FUNCTION op_dba.define_template(
    database_name character varying,
    is_template boolean,
    allow_connections boolean)
  RETURNS void AS
$BODY$
DECLARE
template_action varchar(255);
conn_action varchar(255);
BEGIN
if quote_ident(database_name) in ('postgres','template0','template1')
then
raise notice 'Action on % is not allowed',quote_literal(database_name);
else
template_action:='UPDATE pg_database SET datistemplate='|| quote_literal(is_template) ||' WHERE datname='|| quote_literal(database_name) ;
conn_action:='UPDATE pg_database SET datallowconn='|| quote_literal(allow_connections) ||' WHERE datname='|| quote_literal(database_name) ;
EXECUTE template_action;
EXECUTE conn_action;
end if;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION op_dba.define_template(character varying, boolean, boolean)
  OWNER TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.define_template(character varying, boolean, boolean) TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.define_template(character varying, boolean, boolean) TO operational_dba;
REVOKE ALL ON FUNCTION op_dba.define_template(character varying, boolean, boolean) FROM public;



DROP FUNCTION op_dba.revoke_connect_from_db(character varying);

CREATE OR REPLACE FUNCTION op_dba.revoke_connect_from_db(db_name character varying)
  RETURNS void AS
$BODY$
declare
role record;
v_count int;
begin
select count(*)
into v_count
from pg_database
where datname=db_name;
if v_count=0
then
RAISE EXCEPTION 'Database does not exist'  USING HINT ='Please provide a valid database name!';
end if;
for role in (select rolname
             from pg_roles
             where rolname not in ('postgres','operational_dba')
             and rolcanlogin=FALSE
             and has_database_privilege(rolname,'postgres', 'connect')=TRUE)
loop
RAISE NOTICE 'REVOKE CONNECT ON DATABASE % from % ', db_name,role.rolname;
EXECUTE 'REVOKE CONNECT on DATABASE ' || quote_ident(db_name) || ' from '||role.rolname;
end loop;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION op_dba.revoke_connect_from_db(character varying)
  OWNER TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.revoke_connect_from_db(character varying) TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.revoke_connect_from_db(character varying) TO operational_dba;
REVOKE ALL ON FUNCTION op_dba.revoke_connect_from_db(character varying) FROM public;


DROP FUNCTION op_dba.drop_extension(character varying, character varying);

CREATE OR REPLACE FUNCTION op_dba.drop_extension(db_name character varying,option character varying)
RETURNS void AS
$BODY$
begin
if lower(option) in ('cascade','restrict')
then
EXECUTE 'DROP EXTENSION '||quote_ident(db_name)||' '||option;
elsif (length(trim(both ' ' from option)) = 0) or (option is null)
then
EXECUTE 'DROP EXTENSION '||quote_ident(db_name);
else
RAISE EXCEPTION 'Invalid option provided'  USING HINT ='Please use cascade,restrict or null as options.';
end if;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION op_dba.drop_extension(character varying, character varying)
  OWNER TO postgres;


GRANT EXECUTE ON FUNCTION op_dba.drop_extension(character varying, character varying) TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.drop_extension(character varying, character varying) TO operational_dba;
REVOKE ALL ON FUNCTION op_dba.drop_extension(character varying, character varying) FROM public;



DROP FUNCTION op_dba.set_search_path(character varying, character varying);

CREATE OR REPLACE FUNCTION op_dba.set_search_path(dbname character varying,path character varying)
RETURNS void AS
$BODY$
declare
default_path varchar(50);
new_path varchar(4000);
--test_path varchar(200);
begin
default_path:='"$user",public';
if length(trim(both ' ' from path)) = 0
then
new_path:=default_path;
else
new_path:=default_path||','||path;
end if;
EXECUTE 'ALTER DATABASE '||quote_ident(dbname)||' SET search_path to '||''''||new_path||'''';
--test_path:='ALTER DATABASE '||quote_ident(dbname)||' SET search_path to '||''''||new_path||'''';
RAISE NOTICE  'Database default search path set to %', ''''||new_path||'''';
end;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;

GRANT EXECUTE ON FUNCTION op_dba.set_search_path(character varying, character varying) TO postgres;
GRANT EXECUTE ON FUNCTION op_dba.set_search_path(character varying, character varying) TO operational_dba;
REVOKE ALL ON FUNCTION op_dba.set_search_path(character varying, character varying) FROM public;
--commit;
