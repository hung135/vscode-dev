
FROM centos:centos7
#relax...this is root inside the container so we don't have 
#to add unnecessary tools and utils just do get a dev environment to run
USER root
  
RUN yum install -y docker-client readline-devel gcc wget make zlib-devel 
RUN mkdir -p /tmp/postgres
WORKDIR /tmp/postgres
RUN wget --no-check-certificate https://ftp.postgresql.org/pub/source/v14.0/postgresql-14.0.tar.gz
RUN tar -xvf postgresql-14.0.tar.gz
WORKDIR /tmp/postgres/postgresql-14.0
RUN /tmp/postgres/postgresql-14.0/configure --prefix=/usr
RUN make
RUN make install
  
RUN yum install -y perl-devel perl-CPAN  perl-DBD-Pg
RUN curl -L https://cpanmin.us | perl - -- App::cpanminus
RUN cpanm --quiet --notest App::Sqitch@0.9996
RUN yum install -y https://repo.ius.io/ius-release-el7.rpm
RUN yum install -y python36u python36u-libs python36u-devel python36u-pip git
RUN pip3 install --user --upgrade pip wheel 
RUN pip3 install --user pyinstaller  
COPY requirements.txt /tmp/
RUN python3 -m pip install -Ur /tmp/requirements.txt
RUN echo "PS1='\[\033[02;32m\]\u@\H:\[\033[02;34m\]\w\$\[\033[00m\] ' " >>/etc/bashrc
 
#install sqitch
ENTRYPOINT [] 
CMD ["sleep","infinity"]
 